// @flow
import React from 'react';
import { css, StyleSheet } from 'aphrodite';
import { Room, User } from '../../types';

const styles = StyleSheet.create({
  roomSidebar: {
    color: '#ab9ba9',
    background: '#4d394b',
  },

  header: {
    padding: '20px 15px',
    marginBottom: '10px',
    width: '220px',
    background: 'rebeccapurple',
  },

  roomName: {
    fontSize: '24px',
    color: '#fff',
  },

  userList: {
    paddingLeft: '15px',
    listStyle: 'none',
  },

  username: {
    marginTop: '10px',
    position: 'relative',
    paddingLeft: '20px',
    fontSize: '14px',
    fontWeight: '300',
    ':after': {
      position: 'absolute',
      top: '7px',
      left: '0',
      width: '8px',
      height: '8px',
      borderRadius: '50%',
      background: 'rgb(64,151,141)',
      content: '""',
    },
  },

  listHeading: {
    paddingLeft: '15px',
    paddingTop: '15px',    
    paddingBottom: '15px',
    width: '100%',
    fontSize: '13px',
    color:'white',
    textTransform: 'uppercase',
    textDecoration: 'none',
  },

  listAvatar: {
    width: '40px',
    height: '40px',
    borderRadius: '4px',
    marginRight: '10px',
  },

  ListLink: {
    color: '#FFFFFF',
     textDecoration: 'none',
  },
});

type Props = {
  room: Room,
  currentUser: User,
}

function getRoomName(name){
  var strName = "";
  
  if(typeof(name) !== 'string')return "Connecting...";

  if(name.indexOf("@"))strName = name.slice(4, name.indexOf("@"));
  else strName = "Connecting...";

  return strName;
}

const RoomSidebar = ({ room, currentUser }: Props) =>
  <div className={css(styles.roomSidebar)}>
    <div className={css(styles.header)}>

    {console.log("RoomSidebar", room, currentUser)}

      <div className={css(styles.roomName)}>{getRoomName(room.name)}</div>
    </div>

    <a href="#" className={css(styles.ListLink)}><div className={css(styles.listHeading)} >    
      <img src="../sentimental.png" className={css(styles.listAvatar)} role="presentation"/>
      <span>Sentiment Analysis</span>
    </div></a>
         
    <a href="#" className={css(styles.ListLink)}><div className={css(styles.listHeading)}>   
        <img src="../keyword.png" className={css(styles.listAvatar)} role="presentation"/>   
        <span>KeyWords</span>
    </div></a>
    

    <a href="#" className={css(styles.ListLink)}><div className={css(styles.listHeading)} href="#">  
      <img src="../mood.png" className={css(styles.listAvatar)} role="presentation"/>    
      <span>Mood Detection</span>
    </div></a>
  </div>;

export default RoomSidebar;

