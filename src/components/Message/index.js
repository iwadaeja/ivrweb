// @flow
import React from 'react';
import moment from 'moment';
import Avatar from '../Avatar';
import { Message as MessageType } from '../../types';

type Props = {
  message: MessageType,
}

function getRoomName(name)
{
  var str = "IVR";
  if(name.indexOf("@"))
  {
    str = name.slice(4, name.indexOf("@"));
  }
  return str;
}

const Message = ({ message: { text, inserted_at, user, source } }: Props) =>
  <div style={{ display: 'flex', marginBottom: '10px' }} >
    <Avatar name={getRoomName(source)} style={{ marginRight: '10px' }} />
    <div>
      <div style={{ lineHeight: '1.2' }}>
        <b style={{ marginRight: '8px', fontSize: '14px' }}>{getRoomName(source)}</b>
        <time style={{ fontSize: '12px', color: 'rgb(192,192,192)' }}>{moment(inserted_at).format('h:mm A')}</time>
      </div>
      <div>{text}</div>
    </div>
  </div>;

export default Message;
