// @flow
import React from 'react';

type Props = {
  name: string,
  size?: number,
  style?: Object,
}

function getURI(name)
{
  var uri;
   
  if(name !== "IVR")uri = "../caller.png";
  else uri = "../ivr.png";
  return uri;
}

const Avatar = ({ name, size = 40, style }: Props) => {
 
  return (
    <img
      src={getURI(name)}
      alt={name}
      style={{ width: `${size}px`, height: `${size}px`, borderRadius: '4px', ...style }}
    />
  );
};

export default Avatar;
