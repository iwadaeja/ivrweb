import api from '../api';
import store from "../store";

var pagination = {
  page_number: 1,
  page_size: 25,
  total_entries: 1,
  total_pages: 1      
  };

export function fetchUserRooms(dispatch) {
   api.get('/events/sip_call_ids')
  .then((response) => {  
    console.log(response);    
      var data = response.data.map( (val, key) => ({id: key, name: '*', topic:val.sipcallid}));
      var rooms = {data, pagination};
      dispatch({ type: 'FETCH_USER_ROOMS_SUCCESS', rooms });
  })
  .catch(( err) => {
      console.log(err);
      alert("No rooms");
  });
}

export function fetchRooms(params) {
  return (dispatch) => api.get('/events/sip_call_ids', params)
    .then((resp) => {     

      var pages = resp.data.slice( ( params.page - 1 ) * params.page_size, params.page * params.page_size);
      var data = pages.map( (sipcallid, index) => ({
          topic:sipcallid.sipcallid, 
          id: index + (( params.page - 1 ) * params.page_size) +1,
          name: `sip:${sipcallid.sipcallid}`,
          }))

      var pagination = {
        page_number: params.page,
        page_size: params.page_size,
        total_entries: resp.data.length,
        total_pages: Math.ceil(resp.data.length/params.page_size)
      }
      var response = {
          data,
          pagination,
      }
      
      dispatch({ type: 'FETCH_ROOMS_SUCCESS', response });
    });
}

export function createRoom(roominfo, dispatch, router) {
  var rooms = store.getState().rooms.currentUserRooms;
  var response = {
    id: rooms.length,
    name: roominfo.sipFromURI,
    topic:roominfo.sipCallID,
    room:roominfo,
    messages:[]
  };  

  console.log()
  dispatch({ type: 'CREATE_ROOM_SUCCESS', response });
  router.transitionTo(`/r/${response.id}`);
}
