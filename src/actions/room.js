import { reset } from 'redux-form';
import { Presence } from 'phoenix';
import api from '../api';
import store from "../store";

var userinfo={};

var resp = 
{
  messages: [],
  pagination: {total_pages: 1, total_entries: 0, page_size: 25, page_number: 1},
  room: {topic: "", name: userinfo.username, id: 1}
}

var msgbase = {
  id:0,
  inserted_at:"2019-02-01T07:58:53",
  text:"How are you",
  user:userinfo
};

const syncPresentUsers = (dispatch, presences) => {
  const presentUsers = [];
  Presence.list(presences, (id, { metas: [first] }) => first.user)
          .map((user) => presentUsers.push(user));
  dispatch({ type: 'ROOM_PRESENCE_UPDATE', presentUsers });
};

export function connectToChannel(socket, roomId) {
 var currentRoom = store.getState().rooms.currentUserRooms[roomId]; 
 console.log('connectToChannel', currentRoom);
  return (dispatch) => {           
    userinfo = store.getState().session.currentUser; 
    const channel = socket.channel(`event:${currentRoom.topic}`);

    let presences = {};
    channel.on('presence_state', (state) => {
      presences = Presence.syncState(presences, state);
      syncPresentUsers(dispatch, presences);
    });

    channel.on('presence_diff', (diff) => {
      presences = Presence.syncDiff(presences, diff);
      syncPresentUsers(dispatch, presences);
    });

    channel.on('message_created', (message) => {
      dispatch({ type: 'MESSAGE_CREATED', message });
    });

    channel.on('vgw_transcription', msg => 
    {
        if(msg.transcription !== 'vgwPostResponseTimeout'){
         msgbase.id++;
        var msgsource = "sip:IVR@IVR.onsip.com";
        if(msg.sipFromURI === msg.source)msgsource = msg.source;
        var message = 
        {
          ...msgbase,
          inserted_at: msg.time,
          text:msg.transcription,
          user:userinfo,
          source:msgsource,
        }        
        dispatch({ type: 'MESSAGE_CREATED', message }); 
        }
   });

    channel.join().receive('ok', (response) => {
      resp.room.name = currentRoom.name;
      dispatch({ type: 'ROOM_CONNECTED_TO_CHANNEL', resp, channel });
    })
    .receive("error", ({reason}) => console.log("failed join", reason) )
    .receive("timeout", () => console.log("Networking issue. Still waiting..."));
    return false;
  };
}

export function leaveChannel(channel) {
  console.log("leaveChannel", channel);
  return (dispatch) => {
    if (channel) {
      channel.leave();
    }
    dispatch({ type: 'USER_LEFT_ROOM' });
  };
}

export function createMessage(channel, data) {
  return (dispatch) => new Promise((resolve, reject) => {
    channel.push('new_message', data)
      .receive('ok', () => resolve(
        dispatch(reset('newMessage'))
      ))
      .receive('error', () => reject());
  });
}

export function loadOlderMessages(roomId, params) {
  return (dispatch) => {
    dispatch({ type: 'FETCH_MESSAGES_REQUEST' });
    return api.fetch(`/rooms/${roomId}/messages`, params)
      .then((response) => {
        dispatch({ type: 'FETCH_MESSAGES_SUCCESS', response });
      })
      .catch(() => {
        dispatch({ type: 'FETCH_MESSAGES_FAILURE' });
      });
  };
}

export function updateRoom(roomId, data) {
  return (dispatch) => api.patch(`/rooms/${roomId}`, data)
    .then((response) => {
      dispatch({ type: 'UPDATE_ROOM_SUCCESS', response });
    });
}
