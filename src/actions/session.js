import { reset } from 'redux-form';
import { Socket } from 'phoenix';
import api from '../api';
import { createRoom } from './rooms';

const API_URL = process.env.REACT_APP_API_URL;
const WEBSOCKET_URL = API_URL.replace(/(https|http)/, 'wss').replace('/api/v1', '');

function connectToSocket(dispatch, router) {
  const userToken = JSON.parse(localStorage.getItem('token')); 
  const socket = new Socket(`${WEBSOCKET_URL}/socket`, {params:{userToken},});
  socket.connect();  
  dispatch({ type: 'SOCKET_CONNECTED', socket });

  const channel = socket.channel(`event:sipCallID`); 
  channel.on('sipCallID', (response) => {
      console.log('event:sipCallID channel on', response);
      createRoom(response, dispatch, router);
    });

  channel.join()
  .receive('ok', (response) => {
    console.log( 'event:sipCallID channel joined');
  })
  .receive("error", ({reason}) => console.log("failed join", reason) )
  .receive("timeout", () => console.log("Networking issue. Still waiting..."));
}

function setCurrentUser(dispatch, response, router) {
  localStorage.setItem('token', JSON.stringify(response.meta.token));  
  dispatch({ type: 'AUTHENTICATION_SUCCESS', response });
  connectToSocket(dispatch, router);  
}

export function login(data, router) {    
  return (dispatch) => api.post('/sessions', data)
    .then((response) => {
      setCurrentUser(dispatch, response, router);
      dispatch(reset('login'));
      router.transitionTo('/'); 
    })
    .catch((err) => {
      dispatch({ type: 'SHOW_ALERT', message: 'Invalid email or password' });
    });
}

export function signup(data, router) {

  var newdata = {  
    email:data.email,
    password:data.password,
    name:data.username
  }

  return (dispatch) => api.post('/users', newdata)
    .then((response) => {
      setCurrentUser(dispatch, response);
      dispatch(reset('signup'));
      router.transitionTo('/');
    })
    .catch((error) => {
      dispatch({ type: 'SIGNUP_FAILURE', error });
    });
}

export function logout(router) {
  return (dispatch) => api.delete('/sessions')
    .then(() => {
      localStorage.removeItem('token');
      dispatch({ type: 'LOGOUT' });      
      router.transitionTo('/login');
    });
}

export function authenticate() {
  return (dispatch) => {
    dispatch({ type: 'AUTHENTICATION_REQUEST' });
    return api.post('/sessions/refresh')
      .then((response) => {
        setCurrentUser(dispatch, response);
      })
      .catch(() => {
        localStorage.removeItem('token');
        window.location = '/login';
      });
  };
}

export const unauthenticate = () => ({ type: 'AUTHENTICATION_FAILURE' });
