import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import session from './session';
import rooms from './rooms';
import room from './room';
import alert from './alert';

const appReducer = combineReducers({
  form,
  session,
  rooms,
  room,
  alert,
});

export default function (state, action) {
  if (action.type === 'LOGOUT') {
    state = undefined;  //reducers are supposed to return the initial state when they are called with undefined as the first argument, no matter the action
  }
  return appReducer(state, action);
}
