const initialState = {
  all: [],
  currentUserRooms: [],
  newRoomErrors: [],
  pagination: {
    total_pages: 0,
    total_entries: 0,
    page_size: 0,
    page_number: 0,
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case 'FETCH_USER_ROOMS_SUCCESS':
      return {
        ...state,
        currentUserRooms: action.rooms.data,
      };  
    case 'FETCH_ROOMS_SUCCESS':
      return {
        ...state,
        all: action.response.data,
        pagination: action.response.pagination,
      };       
    case 'CREATE_ROOM_SUCCESS':
      return {
        ...state,
        all: [
          action.response,
          ...state.all,
        ],
        currentUserRooms: [
          ...state.currentUserRooms,
          action.response,
        ],
        newRoomErrors: [],
      };
    case 'CREATE_ROOM_FAILURE':
      return {
        ...state,
        newRoomErrors: action.error.errors,
      };
    default:
      return state;
  }
}
