
const API = process.env.REACT_APP_API_URL;

function headers() {
  //const token = JSON.parse(localStorage.getItem('token'));

  return {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  };
}

function parseResponse(response) {   
  return response.json().then((json) => {    
    if (!response.ok) {
      return Promise.reject(json);
    }    
    return json;
  });
}

function queryString(params) {
  const query = Object.keys(params)
                      .map((k) => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
                      .join('&');
  return `${query.length ? '?' : ''}${query}`;
}

export default {
  get(url, params = {}) {
    const token = JSON.parse(localStorage.getItem('token'));    
    return fetch(`${API}${url}${queryString(params)}`, {
      method: 'GET',
      mode: 'cors',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      }
    })
    .then(parseResponse);
  },

  post(url, data) {

    var body = JSON.stringify(data);
    var header  = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      };
      
    if(url === '/users')
    {
        body = JSON.stringify({"user": data});
    }else if(url === '/sessions'){
      header = {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      body = 'email=' + data.email + '&password=' + data.password;
    }
    return fetch(`${API}${url}`, {
      method: 'POST',
      mode: 'cors',
      headers: header,
      body
    })
    .then(parseResponse);
  },

  patch(url, data) {    
    const body = JSON.stringify(data);
    return fetch(`${API}${url}`, {
      method: 'PATCH',
      mode: 'cors',
      headers: headers(),
      body: body
    })
    .then(parseResponse);
  },

  delete(url) {

    const token = JSON.parse(localStorage.getItem('token'));
    return fetch(`${API}${url}`, {
      method: 'DELETE',
      mode: 'cors',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      }
    })
    //.then(parseResponse);
  },
};
